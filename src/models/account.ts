export class Account {
    name: string;
    ammount: string | number;
    expenses: Array<any>
    incomes: Array<any>

    constructor(name: string = "", ammount: string | number = "") {
        this.name = name;
        this.ammount = ammount;
        this.expenses = [];
        this.incomes = [];
    }

}