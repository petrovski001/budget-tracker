import CircleIcon from '@mui/icons-material/Circle';
import { Button, Typography } from '@mui/material';
import { useState } from 'react';
import { useSelector } from 'react-redux';
import { RootState } from '../../redux/reducers/reducer';
import './category-picker.css';

interface CategoryPickerProps {
    setCategory: (category: string) => void
}

const CategoryPicker: React.FC<CategoryPickerProps> = ({setCategory}) => {
    const [clicked, setClicked] = useState<string>('');
    const categories = useSelector((store: RootState) => store.categories)

    const handleChange = (category: string) => {
        setCategory(category);
        setClicked(category);
    }

    const renderCategories = () => {
        return categories.map((category) => {
            return <Button size={'small'}
            variant='outlined'
            key={category.name}
            onClick={(e) => handleChange(category.name)}
            sx={category.name === clicked ? {backgroundColor: '#B8E0E6'} : {}}
            startIcon={<CircleIcon sx={{ color: category.color }} />}>
                {category.name}
            </Button>
        })
    }

    return (
        <div className='category'>
            <div className='heading'>
                <Typography variant="caption" display="block" gutterBottom>Select category:</Typography>
            </div>
            <div className="category-container">
                {renderCategories()}
            </div>
        </div>
    );
}

export default CategoryPicker;