import {combineReducers} from 'redux';
import { CategoryReducer } from './category';
import { AccountsReducer } from './accounts';

export const rootReducer = combineReducers({categories: CategoryReducer, accounts: AccountsReducer});

export type RootState = ReturnType<typeof rootReducer>