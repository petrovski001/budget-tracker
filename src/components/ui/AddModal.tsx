import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  FormControl,
  InputLabel,
  MenuItem,
  Select,
  SelectChangeEvent,
  TextField,
  Typography,
} from "@mui/material";
import IconButton from "@mui/material/IconButton";
import CloseIcon from "@mui/icons-material/Close";
import DatePicker from "./DatePicker";
import { ChangeEvent, useState } from "react";
import CategoryPicker from "../category-picker/CategoryPicker";
import { useDispatch, useSelector } from "react-redux";
import { ADD_EXPENSE, ADD_INCOME } from "../../redux/actions/accounts";
import { RootState } from "../../redux/reducers/reducer";
import { account } from "../../services/types";

type AddModalProps = {
  open: boolean;
  handleModal: (isOpen: boolean) => void;
};

export default function AddModal({ open, handleModal }: AddModalProps) {
  const [date, setDate] = useState<Date | null>(null);
  const [amount, setAmount] = useState<number>(0);
  const [category, setCategory] = useState<string | null>(null);
  const [account, setAccount] = useState<string>("");
  const accounts = useSelector((store: RootState) => store.accounts);
  const dispatch = useDispatch();

  const closeModal = () => handleModal(false);

  const handleChange = (event: ChangeEvent<HTMLInputElement>) => {
    const value = event.target.valueAsNumber;
    !isNaN(value) ? setAmount(value) : setAmount(0);
  };

  const handleAccountChange = (event: SelectChangeEvent<string>) => {
    const { value } = event.target;
    if (value !== "") {
      console.log(value);
      setAccount(value);
    }
  };

  const addExpense = () => {
    const action = amount > 0 ? ADD_INCOME : ADD_EXPENSE;
    const payload = {
      type: action,
      payload: {
        name: account,
        date: date?.toDateString(),
        category: category,
        amount: amount,
      },
    };

    console.log("payload", payload);
    if (date != null && amount !== 0 && category !== null) {
      dispatch(payload);
      console.log("dispatched", payload);
      setDate(null);
      setAmount(0);
      setCategory(null);
      closeModal();
    }
  };

  return (
    <Dialog open={open} onClose={closeModal}>
      <DialogTitle>
        Add Income/Expense
        <IconButton
          aria-label="close"
          onClick={closeModal}
          sx={{
            position: "absolute",
            right: 8,
            top: 8,
            color: (theme) => theme.palette.grey[500],
          }}
        >
          <CloseIcon />
        </IconButton>
      </DialogTitle>
      <DialogContent>
        <br />
        <FormControl fullWidth>
          <InputLabel id="account">Choose account</InputLabel>
          <Select
            labelId="account"
            id="account"
            value={account}
            onChange={handleAccountChange}
            fullWidth
            label="Choose Account"
          >
            {accounts.map((account: account) => {
              return (
                <MenuItem key={account.name} value={account.name}>
                  <Typography variant="body1" sx={{ flexGrow: 1 }}>
                    {account.name}
                  </Typography>
                  <Typography
                    variant="caption"
                    sx={{ color: account.ammount > 0 ? "green" : "red" }}
                  >
                    {account.ammount > 0 ? "+" : ""}
                    {account.ammount}
                  </Typography>
                </MenuItem>
              );
            })}
          </Select>
        </FormControl>
        <DatePicker
          label="Date of expense/income"
          date={date}
          setDate={setDate}
        />
        <TextField
          margin="dense"
          label="Amount"
          type="number"
          fullWidth
          value={amount}
          onChange={handleChange}
          sx={{ marginTop: "1rem" }}
          variant="outlined"
        />
        <CategoryPicker setCategory={setCategory} />
      </DialogContent>
      <DialogActions>
        <Button onClick={closeModal}>Cancel</Button>
        <Button onClick={addExpense}>Add</Button>
      </DialogActions>
    </Dialog>
  );
}
