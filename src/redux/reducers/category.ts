import { category } from "../../services/types";
import { ADD_CATEGORY } from "../actions/category";

type payloadOptions = {
  type: string;
  payload: category;
};

type State = category[];

const initialState: category[] = [{ name: 'Home', color: 'red' },
    { name: 'Food & Drinks', color: 'orange' },
    { name: 'Transport', color: 'lightblue' },
    { name: 'Salary', color: 'green' },
    { name: 'Travel', color: 'yellow' },
    { name: 'Shopping', color: 'pink' },
    { name: 'Other', color: 'purple' },
    ];

export const CategoryReducer = (state: State = initialState, action: payloadOptions) => {
  switch (action.type) {
    case ADD_CATEGORY:
      return [...state, action.payload];
    default:
      return state;
  }
};
