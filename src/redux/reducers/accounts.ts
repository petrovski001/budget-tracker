import { expense, account, income } from "../../services/types";
import { ADD_ACCOUNT, ADD_EXPENSE, ADD_INCOME } from "../actions/accounts";

type AccountPayload =
  | { type: "ADD_ACCOUNT"; payload: account }
  | { type: "ADD_INCOME"; payload: income }
  | { type: "ADD_EXPENSE"; payload: expense };

type State = account[];

const initialState: account[] = [];

export const AccountsReducer = (
  state: State = initialState,
  action: AccountPayload
) => {
  switch (action.type) {
    case ADD_ACCOUNT:
      return [...state, action.payload];
    case ADD_INCOME:
      const account = state.find((a) => a.name === action.payload.name);
      if (account) {
        const income = { ...action.payload } as income;
        delete income["name"];
        account.incomes.push(income);
        return [...state];
      }
      return [...state];
    case ADD_EXPENSE:
      const acc = state.find((a) => a.name === action.payload.name);
      if (acc) {
        const expense = { ...action.payload } as expense;
        delete expense["name"];
        acc.expenses.push(expense);
        return [...state];
      }
      return [...state];
    default:
      return state;
  }
};
