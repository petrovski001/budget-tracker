export type category = {
    name: string;
    color: string;
}

export type expense = {
    name?: string,
    date: string,
    amount: number,
    category: category
}

export interface income {
    name?: string,
    date: string,
    amount: number,
    category: category
}

export type account = {
    name: string,
    ammount: number | string,
    incomes: income[],
    expenses: expense[]
}