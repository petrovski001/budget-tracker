import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import { useSelector } from "react-redux";
import { RootState } from "../../redux/store";

export default function AccountsTable() {
  const accounts = useSelector((state: RootState) => state.accounts);

  return (
    <TableContainer>
      <Table sx={{ minWidth: 300 }} aria-label="simple table">
        <TableHead>
          <TableRow>
            <TableCell>
              <b>Account name</b>
            </TableCell>
            <TableCell align="right">
              <b>Balance</b>
            </TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {accounts.map((row) => 
            <TableRow key={row.name}>
              <TableCell component={'th'} scope='row'>{row.name}</TableCell>
              <TableCell align='right' sx={{color: row.ammount > 0 ? 'green' : 'red'}}>{row.ammount}</TableCell>
            </TableRow>
          )}
        </TableBody>
      </Table>
    </TableContainer>
  );
}
