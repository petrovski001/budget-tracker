import LocalizationProvider from "@mui/lab/LocalizationProvider";
import AdapterDateFns from "@mui/lab/AdapterDateFns";
import { DatePicker as DPicker } from "@mui/lab";
import { TextField } from "@mui/material";

type DatePickerProps = {
  date: Date | null;
  setDate: (date: Date | null) => void;
  label: string;
};

export default function DatePicker({ date, setDate, label }: DatePickerProps) {
  // const [value, setValue] = React.useState<Date | null>(null);

  const handleChange = (newDate: Date | null) => setDate(newDate);

  return (
    <div style={{marginTop: '1rem'}}>
      <LocalizationProvider dateAdapter={AdapterDateFns}>
        <DPicker
          label={label}
          value={date}
          onChange={handleChange}
          renderInput={(params) => (
            <TextField {...params} variant="outlined" fullWidth />
          )}
        />
      </LocalizationProvider>
    </div>
  );
}
