import { Box, Button, Container, Paper, Typography } from "@mui/material";
import { useState } from "react";
import AccountsTable from "../components/accounts/AccountsTable";
import AddAccountModal from "../components/accounts/AddAccountModal";

function AccountView() {
  const [open, setOpen] = useState(false);

  return (
    <Container fixed>
      <AddAccountModal open={open} handleModal={setOpen} />
      <Paper
        elevation={0}
        variant="outlined"
        sx={{ paddingTop: "1px", paddingBottom: "1px" }}
      >
        <Box sx={{ display: "flex", justifyContent: "space-between" }}>
          <Typography variant="h6" margin={2}>
            Accounts
          </Typography>
          <Button variant="contained" sx={{margin: '1rem'}}onClick={() => setOpen(true)}>Add account</Button>
        </Box>
      </Paper>
      <Paper elevation={1} sx={{ marginTop: "1rem" }}>
        <AccountsTable />
      </Paper>
    </Container>
    // </Box>
  );
}

export default AccountView;
