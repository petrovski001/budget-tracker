import { Fragment, useState } from "react";
import { Box, createTheme } from "@mui/material";
import Navbar from "./components/ui/AppDrawer";
import "./App.css";
import { Route, Routes } from "react-router-dom";
import AccountView from "./views/AccountView";
import { ThemeProvider } from "@emotion/react";

function App() {
  const [modal, setModal] = useState(false);

  const toggleModal = (modal: boolean) => {
    setModal(modal);
  };

  const theme = createTheme({
    palette: {
      background: {
        default: "#f7f9fc"
      }
    },
  });

  return (
    <Box sx={{ display: "flex" }}>
      <ThemeProvider theme={theme}>
        <Navbar modal={modal} setModal={toggleModal}>
          <Fragment>
            <Routes>
              <Route path="/" element={<p>home</p>} />
              <Route path="/accounts" element={<AccountView />} />
            </Routes>
          </Fragment>
        </Navbar>
      </ThemeProvider>
    </Box>
  );
}

export default App;
