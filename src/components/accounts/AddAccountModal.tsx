import {
  AttachMoneyRounded,
  Close,
  CreditCardRounded,
} from "@mui/icons-material";
import {
  Dialog,
  DialogTitle,
  IconButton,
  DialogContent,
  DialogContentText,
  DialogActions,
  Button,
  InputAdornment,
  TextField,
} from "@mui/material";
import { ChangeEvent, useState } from "react";
import { useDispatch } from "react-redux";
import { Account } from "../../models/account";
import { ADD_ACCOUNT } from "../../redux/actions/accounts";

interface AddAccountModalProps {
  open: boolean;
  handleModal: (open: boolean) => void;
}

export default function AddAccountModal({
  open,
  handleModal,
}: AddAccountModalProps) {
  const [account, setAccount] = useState<Account>(new Account());
  const dispatch = useDispatch();

  const closeModal = () => {
    handleModal(false);
  };

  const addAccount = () => {
    if (account.name !== "") {
      const newAccount = { ...account, incomes: [], expenses: [] };
      dispatch({ type: ADD_ACCOUNT, payload: newAccount });
      setAccount(new Account());
      handleModal(false);
    }
  };

  const handleChange = (event: ChangeEvent<HTMLInputElement>) => {
    const { name, value } = event.target;
    setAccount({ ...account, [name]: value });
  };

  return (
    <Dialog open={open} onClose={() => handleModal(false)}>
      <DialogTitle>
        Add account
        <IconButton
          aria-label="close"
          onClick={closeModal}
          sx={{
            position: "absolute",
            right: 8,
            top: 8,
            color: (theme) => theme.palette.grey[500],
          }}
        >
          <Close />
        </IconButton>
      </DialogTitle>
      <DialogContent>
        <DialogContentText></DialogContentText>
        <TextField
          fullWidth
          margin="normal"
          variant="outlined"
          label="Account Name"
          name="name"
          value={account.name}
          onChange={handleChange}
          InputProps={{
            startAdornment: (
              <InputAdornment position="start">
                <CreditCardRounded />
              </InputAdornment>
            ),
          }}
        />
        <TextField
          label="Balance"
          margin="normal"
          name="ammount"
          value={account.ammount}
          fullWidth
          variant="outlined"
          onChange={handleChange}
          InputProps={{
            startAdornment: (
              <InputAdornment position="start">
                <AttachMoneyRounded />
              </InputAdornment>
            ),
          }}
        />
      </DialogContent>
      <DialogActions>
        <Button onClick={closeModal}>Cancel</Button>
        <Button onClick={addAccount}>Add</Button>
      </DialogActions>
    </Dialog>
  );
}
