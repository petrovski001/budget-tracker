import {composeWithDevTools} from 'redux-devtools-extension';
import { applyMiddleware, createStore } from 'redux';
import { rootReducer } from './reducers/reducer';
import thunk from 'redux-thunk';

const initialstate={};
const middleware = [thunk];

const store = createStore(rootReducer,initialstate,composeWithDevTools(applyMiddleware(...middleware)))

export default store;

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;